package edu.upenn.cis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.upenn.cis.cis455.crawler.Data.Channel;
import edu.upenn.cis.cis455.crawler.ms2.*;
import edu.upenn.cis.cis455.crawler.utils.GLOBAL;
import edu.upenn.cis.cis455.crawler.utils.PageQueue;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageServer;
import edu.upenn.cis.stormlite.*;
import edu.upenn.cis.stormlite.PrintBolt;
import org.junit.Test;

import java.util.Date;

public class crawlerSpoutTest {
    private static final String CrawlerQueue_SPOUT = "CrawlerQueue_SPOUT";
    private static final String DOC_BOLT = "DOC_BOLT";
    private static final String LinkExtractor_BOLT = "LinkExtractor_BOLT";
    private static final String PRINT_BOLT = "PRINT_BOLT";


    @Test
    public void cralwerSpoutNextTest() throws Exception{
        GLOBAL.startUrl = "https://crawltest.cis.upenn.edu";
        GLOBAL.count = 3;
        GLOBAL.size = 1000000;
        GLOBAL.envPath = "./database";
        StorageServer database = (StorageServer) StorageFactory.getDatabaseInstance("./database");
        database.truncateDocumentDatabase();
        database.truncateMd5CidDatabase();
        Channel Channel = new Channel();
        Channel.setxPath("/rss/channel/item");
        Channel.setName("testxPath2");
        Channel.setCreateTime(new Date());
        Channel.setCreatedBy("Developer");

        Channel Channel2 = new Channel();
        Channel2.setxPath("/html/body/h1");
        Channel2.setName("HTML");
        Channel2.setCreateTime(new Date());
        Channel2.setCreatedBy("Developer");

        database.DAO.xPath.put(Channel);
        database.DAO.xPath.put(Channel2);

        PageQueue.pagesToVisit().add(GLOBAL.startUrl);
        Config config = new Config();

        CrawlerQueueSpout spout = new CrawlerQueueSpout();
        DocumentFetcherBolt docbolt = new DocumentFetcherBolt();
        LinkExtractorBolt linkbolt = new LinkExtractorBolt();

        PrintBolt printer = new PrintBolt();
        PrintBolt printer2 = new PrintBolt();

        DOMParserBolt domParserBolt = new DOMParserBolt();
        PathMatcherBolt pathMatcherBolt = new PathMatcherBolt();

        // wordSpout ==> countBolt ==> MongoInsertBolt
        TopologyBuilder builder = new TopologyBuilder();

        // Only one source ("spout") for the words
        builder.setSpout(CrawlerQueue_SPOUT, spout, 1);

        builder.setBolt(DOC_BOLT, docbolt, 3).shuffleGrouping(CrawlerQueue_SPOUT);

        builder.setBolt(LinkExtractor_BOLT, linkbolt, 2).shuffleGrouping(DOC_BOLT);

        builder.setBolt("DOM_BOLT", domParserBolt, 2).shuffleGrouping(DOC_BOLT);

        // A single printer bolt (and officially we round-robin)
        builder.setBolt(PRINT_BOLT, printer, 4).shuffleGrouping(LinkExtractor_BOLT);

//        builder.setBolt("PRINT_BOLT2", printer2, 4).shuffleGrouping("DOM_BOLT");
        builder.setBolt("PATH_MATCH_BOLT", pathMatcherBolt, 5).shuffleGrouping("DOM_BOLT");

        LocalCluster cluster = new LocalCluster();
        Topology topo = builder.createTopology();

        ObjectMapper mapper = new ObjectMapper();
        try {
            String str = mapper.writeValueAsString(topo);

            System.out.println("The StormLite topology is:\n" + str);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        cluster.submitTopology("test", config,
                builder.createTopology());

        while (GLOBAL.count > PageQueue.pageVisited().size())
            try {
//                System.out.println("***SLEEPING "+ PageQueue.pageVisited().size());
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        database.printDocumentInDb();

        for(Channel p : database.DAO.xPath.map().values()) {
            System.out.println(p.getName() + ":" + p.getxPath());
            System.out.println(p.getDocumentIdSet().toString());

        }
        cluster.killTopology("test");
        cluster.shutdown();
    }
}
