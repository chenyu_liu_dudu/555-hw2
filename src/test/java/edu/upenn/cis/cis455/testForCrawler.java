package edu.upenn.cis.cis455;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.cis455.crawler.utils.GLOBAL;
import edu.upenn.cis.cis455.crawler.utils.RobotFileChecker;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageServer;
import org.junit.Assert;
import org.junit.Test;
import spark.utils.IOUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import static edu.upenn.cis.cis455.crawler.utils.GLOBAL.USER_AGENT;
import static edu.upenn.cis.cis455.crawler.utils.RobotFileChecker.pickMyRobot;

public class testForCrawler {

//    @Test
//    public void testForCrawler() {
//        GLOBAL.startUrl = "https://crawltest.cis.upenn.edu/";
//        GLOBAL.count = 2;
//        GLOBAL.size = 1000000;
//        GLOBAL.envPath= "./database";
//        StorageServer database = (StorageServer) StorageFactory.getDatabaseInstance();
////        database.truncateDocumentDatabase();
//
//        Crawler crawler = new Crawler(database);
//        crawler.start();
//        database.printDocumentInDb();
//    }


    @Test
    public void testRoboUrl() {
        String roboUrl  = helperFunction.generateRobotUrl("https://crawltest.cis.upenn.edu/misc/weather.xml");
        assert(roboUrl.equals("https://crawltest.cis.upenn.edu/robots.txt"));
    }



    @Test
    public void testParser() {
        String robotFile = helperFunction.generateRobotUrl("https://crawltest.cis.upenn.edu/misc/weather.xml");
        String[] split_file = robotFile.split("\\n\\n");
        for(String s : split_file) {
            String[] line = s.split("\n");
            RobotFileChecker r = new RobotFileChecker();
            for (String l: line) {
                String [] token = l.split(":");
                if(token.length == 1) continue;
                String value = token[1].trim();
                switch (token[0].toLowerCase()) {
                    case ("user-agent"):
                        r.setUserAgent(value);
                        break;
                    case("disallow"):
                        r.disallowList.add(value);
                        break;
                    case("crawl-delay"):
                        r.setCrawlDelay(Integer.parseInt(value));
                        break;
                }
            }
            System.out.println(r.toString());

        }
    }

    @Test
    public void testPickMyRobot() throws Exception{
        RobotFileChecker r = pickMyRobot(USER_AGENT,"https://crawltest.cis.upenn.edu/misc/weather.xml");
        System.out.println(r.toString());
        Assert.assertTrue(r.getUserAgent().equals("cis455crawler"));
    }

    @Test
    public void robotCanAccessTest() throws Exception{
        RobotFileChecker r = pickMyRobot(USER_AGENT,"https://crawltest.cis.upenn.edu/misc/weather.xml");
        Assert.assertFalse(r.urlIsDisallowed("https://crawltest.cis.upenn.edu/misc/weather.xml"));
        Assert.assertTrue(r.urlIsDisallowed( "https://crawltest.cis.upenn.edu/marie/private/"));
    }

    @Test
    public void testUrlInfo() {
        URLInfo urlInfo = new URLInfo("https://crawltest.cis.upenn.edu/misc/weather.xml");
        Assert.assertTrue(urlInfo.getHostName().equals("crawltest.cis.upenn.edu"));
        Assert.assertTrue(urlInfo.getFilePath().equals("/misc/weather.xml"));
        Assert.assertTrue(urlInfo.getPortNo() == 443);
        Assert.assertTrue(urlInfo.isSecure() == true);

    }
    @Test
    public void testStartWith() {
        String url  = "https://crawltest.cis.upenn.edu/marie/private";
        String disallow = "https://crawltest.cis.upenn.edu/marie/private/";
        Assert.assertTrue(url.startsWith(disallow.substring(0, disallow.length()-1)));
    }

    @Test
    public void testGetDocument() {
        String url  = "https://crawltest.cis.upenn.edu/";
        try {
            URL u = new URL(url);

            URLConnection conn = u.openConnection();
            conn.setConnectTimeout(5000);
            conn.setReadTimeout(5000);
            conn.connect();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            IOUtils.copy(conn.getInputStream(), baos);

            byte[] byteArray = (baos.toByteArray());
            System.out.println(new String(byteArray));

        }
        catch (IOException e)
        {
            // Log error and return null, some default or throw a runtime exception
        }

    }

    @Test
    public void getResponseFromJsonURL() {
        String url  = "https://developer.mozilla.org/";

        try {
            /************** For getting response from HTTP URL start ***************/
            URL object = new URL(url);

            HttpURLConnection connection = (HttpURLConnection) object
                    .openConnection();
            // int timeOut = connection.getReadTimeout();
            connection.setReadTimeout(60 * 1000);
            connection.setConnectTimeout(60 * 1000);
            int responseCode = connection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStr = connection.getInputStream();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                IOUtils.copy(inputStr, baos);
                byte[] byteArray = (baos.toByteArray());
                System.out.println(new String(byteArray));

                /************** For getting response from HTTP URL end ***************/

            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }
}
