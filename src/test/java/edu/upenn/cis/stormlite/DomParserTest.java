package edu.upenn.cis.stormlite;

import edu.upenn.cis.cis455.helperFunction;
import edu.upenn.cis.stormlite.tuple.Values;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class DomParserTest {

    @Test
    public void testXmlParser() {
        String rawml = helperFunction.fetchDoc("https://crawltest.cis.upenn.edu/international/corriere.xml");
        Document doc = Jsoup.parse(rawml, "", Parser.xmlParser());
        Elements elements = doc.getAllElements();
        ArrayList all = new ArrayList();
        for (Element element : elements) {
            if (!element.ownText().isEmpty()) {
                StringBuilder path = new StringBuilder(element.nodeName());
                String value = element.ownText();
                Elements p_el = element.parents();
                for (Element el : p_el) {
                    path.insert(0, el.nodeName() + '/');
                }
                all.add(path + "=" + value);
            }
        }
        Assert.assertTrue(all.contains("rss/channel/item/category=Spettacoli"));
        Assert.assertTrue(all.contains("rss/channel/item/pubDate=Wed, 1 Feb 2006 12:28:03 GMT"));
        Assert.assertTrue(all.contains("rss/channel/title=Corriere.it - Cronache"));
        Assert.assertTrue(all.contains("rss/channel/link=http://www.corriere.it/"));
        Assert.assertTrue(all.contains("rss/channel/description=Corriere della sera online"));
        Assert.assertTrue(all.contains("rss/channel/item/pubDate=Wed, 1 Feb 2006 12:28:03 GMT"));
    }

    @Test
    public void testHtmlParser() {
        String rawxml = helperFunction.fetchDoc("https://crawltest.cis.upenn.edu/marie/tpc/");
        Document doc = Jsoup.parse(rawxml, "", Parser.htmlParser());
        Elements elements = doc.getAllElements();
        ArrayList all = new ArrayList();
        for (Element element : elements) {
            if (!element.ownText().isEmpty()) {
                StringBuilder path = new StringBuilder(element.nodeName());
                String value = element.ownText();
                Elements p_el = element.parents();
                for (Element el : p_el) {
                    path.insert(0, el.nodeName() + '/');
                }
                all.add(path + "=" + value);
            }
        }
        Assert.assertTrue(all.contains("html/head/title=Testing data"));
        Assert.assertTrue(all.contains("html/body/h2=Testing Data"));
        Assert.assertTrue(all.contains("html/body/ul/li/a=Customers"));
        Assert.assertTrue(all.contains("html/body/ul/li/a=Nation"));
        Assert.assertTrue(all.contains("html/body/ul/li/a=Part"));
    }

}
