package edu.upenn.cis.stormlite;

import edu.upenn.cis.cis455.helperFunction;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.XmlDeclaration;
import org.jsoup.parser.Parser;
import org.junit.Test;

import java.io.IOException;

public class DocumentFetcherTest {
    String u1 = "https://crawltest.cis.upenn.edu/cnn/cnn_world.rss.xml";
    String u2 = "https://crawltest.cis.upenn.edu/international/peoplesdaily_world.xml";
    String u3 = "https://crawltest.cis.upenn.edu/misc/weather.xml";
    @Test
    public void testForeignPage() throws IOException {
        Connection.Response response  = Jsoup
                .connect(u1)
                .method(Connection.Method.GET)
                .execute();
        if (response.contentType().contains("text/html")) {

            System.out.println("html doc\n" + response.body());
        }
        else {
            Document doc = Jsoup.parse(response.body(), "", Parser.xmlParser());
            XmlDeclaration decl = (XmlDeclaration) doc.childNode(0);
            System.out.println("xml doc\n");

//            System.out.println(decl.attr("encoding"));
            response.charset(decl.attr("encoding") != null ? decl.attr("encoding") : "UTF-8");
//            response.charset("ISO-8859-1");
            System.out.println("\n" + response.body());
        }
    }

    @Test
    public void testFetchDoc() {
        String d = helperFunction.fetchDoc(u3);
        System.out.println(d);
    }


}
