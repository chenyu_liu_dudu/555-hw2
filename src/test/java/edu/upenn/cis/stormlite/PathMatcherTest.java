package edu.upenn.cis.stormlite;

import edu.upenn.cis.cis455.crawler.ms2.PathMatcherBolt;
import edu.upenn.cis.cis455.helperFunction;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageServer;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static edu.upenn.cis.cis455.crawler.ms2.PathMatcherBolt.isMatch;


public class PathMatcherTest {
    @Test
    public void MatcherTest1 () {
        String xml = "<rss>hi<channel><title>hello</title></channel></rss>";

        String[] XPath = {"/rss[text() = \"hi\"]/channel",
                "/rss[contains(text(),\"hi\")]",
                "/rss[contains(text(),\"hi\")]/channel",
                "/rss[contains(text(),\"hi\")]/channel/title"};

        String[] xpathFalse = {"/rss[contains(text(), \"noo\")]/channel",
        "/rss/channel[contains(text(), \"hello\")]"};
//        String xml = "<foo><bar>two<baz>three</baz></bar>one</foo>";
//        String[] XPath = {"/foo[text()=\"one\"]/bar[contains(text(), \"wo\")]"};

        Document doc = Jsoup.parse(xml, "", Parser.xmlParser());
        Elements elements = doc.getAllElements();
        ArrayList<String> all = new ArrayList();
        for (Element element : elements) {
            if (!element.ownText().isEmpty()) {
                StringBuilder path = new StringBuilder(element.nodeName());
                String value = element.ownText();
                Elements p_el = element.parents();
                for (Element el : p_el) {
                    if(el.ownText().isEmpty()) {
                        path.insert(0, el.nodeName()  + '/');
                    } else {
                        path.insert(0, el.nodeName()+ "="+el.ownText()  + '/');
                    }
                }
                all.add(path  + "=" + value );
            }
        }
        for (int i=0;i<all.size();i++) {
            for (String s: XPath) {
                System.out.println("event{"+ all.get(i)+"}");
                System.out.println("path{"+ s+"}");
                System.out.println(isMatch(s, all.get(i)));
            }
        }

    }


    @Test
    public void pathMatcherContainTest() {
        String event = "a/b/c/d=theEntireText";
        String xpath = "/a/b/c[contains(text(),\"theEntireText\")";
        Assert.assertFalse(isMatch(xpath, event));

        event = "a/b/c=theEntireText";
        Assert.assertTrue(isMatch(xpath, event));

        event = "a/b/c=theEntireText andothers";
        Assert.assertTrue(isMatch(xpath, event));
    }


    @Test
    public void pathMatcherExactTextTest() {
        String event = "a/b/c/d=theEntireText";
        String xpath = "/a/b/c[(text()=\"theEntireText\")";
        Assert.assertFalse(isMatch(xpath, event));

        event = "a/b/c=theEntireText";
        Assert.assertTrue(isMatch(xpath, event));

        event = "a/b/c=theEntireText andothers";
        Assert.assertFalse(isMatch(xpath, event));
    }
}
