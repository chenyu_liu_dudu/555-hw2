package edu.upenn.cis.cis455.crawler.utils;

import java.util.ArrayList;
import java.util.Date;

import static edu.upenn.cis.cis455.helperFunction.fetchRobotsFile;

public class RobotFileChecker {
    private String userAgent;
    public ArrayList<String> disallowList = new ArrayList<>();
    private int crawlDelay;
    private long lastAccessTime = -1;

    /**
     * pickMyRobot will create a robot based on myUserAgent and
     * robots.txt file specified by the url's domain
     *
     * @param myUserAgent
     * @param url
     * @return best fit robot
     * @throws Exception
     */
    public static RobotFileChecker pickMyRobot(String myUserAgent, String url) throws NullPointerException{
        RobotFileChecker myRobot = new RobotFileChecker();
        String robotFile = fetchRobotsFile(url);
        if (robotFile == null) throw new NullPointerException();
        String[] split_file = robotFile.split("\\n\\n");

        URLInfo urlInfo = new URLInfo(url);
        for(String s : split_file) {
            RobotFileChecker r = new RobotFileChecker();
            for (String l: s.split("\n")) {
                String [] token = l.split(":");
                if(token.length == 1) continue;
                String value = token[1].trim();
                switch (token[0].toLowerCase()) {
                    case ("user-agent"):
                        r.setUserAgent(value);
                        break;
                    case("disallow"):
                        String disallowurl = urlInfo.isSecure() ? "https://" + urlInfo.getHostName() + value : "http://" + urlInfo.getHostName() + value;
                        r.disallowList.add(disallowurl);
                        break;
                    case("crawl-delay"):
                        r.setCrawlDelay(Integer.parseInt(value));
                        break;
                }
            }
            if(r.getUserAgent().equals(myUserAgent)) return r;
            else if (r.getUserAgent().equals("*")) myRobot = r;
        }
        return myRobot;
    }

    /**
     *
     * robotCanAcess instantiate a robot identity based on the robots.txt specified by the url's domain
     * then it compares this url with the disallowList of the robot
     * @param url
     * @return true if url can be crawled
     */
    public boolean urlIsDisallowed(String url) {
        for (String disallowUrl : this.disallowList) {
            if (url.startsWith(disallowUrl.substring(0, disallowUrl.length()-1))){
                return true;
            }
        }
        return false;

    }

    public String toString() {
        return "[userAgent: " + userAgent +
                " disallowList: " + disallowList.toString() +
                " crawlDelay: " + crawlDelay +
                " lastAccess: " + lastAccessTime +  "]";
    }

    public synchronized boolean isDelayPass() {
        if(this.crawlDelay == 0) return true;
        else if ( this.getLastAccessTime()!= -1
                && new Date().getTime() - this.getLastAccessTime() < this.getCrawlDelay() * 1000) {
            return false;
        }
        System.out.println("***IsDelayPass");

        return true;

    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public int getCrawlDelay() {
        return crawlDelay;
    }

    public void setCrawlDelay(int crawlDelay) {
        this.crawlDelay = crawlDelay;
    }

    public long getLastAccessTime() {
        return lastAccessTime;
    }

    public void setLastAccessTime(long lastAccessTime) {
        this.lastAccessTime = lastAccessTime;
    }


}
