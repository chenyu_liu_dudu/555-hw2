package edu.upenn.cis.cis455.crawler.utils;

import java.util.HashMap;
import java.util.Map;

public class RobotInfo {
    private static Map<String, RobotFileChecker> robotsMap = new HashMap<>(); //mapping from url domain to RobotFileChecker
    public static Map<String, RobotFileChecker> getRobotsMap() {
        return robotsMap;
    }
}
