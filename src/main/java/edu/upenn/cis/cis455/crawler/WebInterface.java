package edu.upenn.cis.cis455.crawler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import static spark.Spark.*;

import edu.upenn.cis.cis455.crawler.Data.Channel;
import edu.upenn.cis.cis455.crawler.Data.Document;
import edu.upenn.cis.cis455.crawler.handlers.LoginFilter;
import edu.upenn.cis.cis455.crawler.handlers.LogoutHandler;
import edu.upenn.cis.cis455.crawler.handlers.RegistrationHandler;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.crawler.handlers.LoginHandler;
import edu.upenn.cis.cis455.storage.StorageServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WebInterface {
    static Logger logger = LogManager.getLogger(WebInterface.class);
    public static void main(String args[]) {
        if (args.length < 1 || args.length > 2) {
            System.out.println("Syntax: WebInterface {path} {root}");
            System.exit(1);
        }

        if (!Files.exists(Paths.get(args[0]))) {
            try {
                Files.createDirectory(Paths.get(args[0]));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        port(45555);
        StorageInterface database = StorageFactory.getDatabaseInstance(args[0]);
        LoginFilter testIfLoggedIn = new LoginFilter(database);

        if (args.length == 2) {
            staticFiles.externalLocation(args[1]);
            staticFileLocation(args[1]);
        }


//        before("/*", "*/*", testIfLoggedIn);
        after("/lookup", (request, response) -> {
            logger.info("after filter");
            response.cookie("ckie1", "afterlookup");
//            return;
        });

        post("/register", new RegistrationHandler(database));
        post("/login", new LoginHandler(database));
        post("/logout", new LogoutHandler());

        get("/logout", new LogoutHandler());
        get("/", (request, response) -> {
            try {
                if (!request.session().attributes().isEmpty()) {
                    String user = request.session().attribute("user");

                    String list = "<ol>";
                    for(Channel channel: ((StorageServer) database).DAO.xPath.map().values()) {
                        list +=  "  <li>"+
                                "<a href="+"/show?channel="+channel.getName()+">"+channel.getName()+"</a>\n"+"</li>\n";
                    }
                    list += "</ol>";
                    return "Welcome " + user + list;
                }
            } catch (IllegalStateException e) { }
            return "hello";
        });
        get("/lookup", (request, response) -> {
            String url = request.queryParams("url");
            logger.info("lookup url = " + url);
            logger.info("url content is " + database.getDocument(url));
            if ( database.getDocument(url) ==null) {
                response.status(404);
                return "404 page not found";
            }
            else {
                response.type(((StorageServer) database).getDocumentObject(url).getContentType());
                return database.getDocument(url);
            }

        });
        get("/create/:name", (request, response) -> {
            String name = request.params("name");
            String xpath = request.queryParams("xpath");
            Channel Channel = new Channel();
            Channel.setxPath(xpath);
            Channel.setName(name);
            Channel.setCreateTime(new Date());
            Channel.setCreatedBy(request.session().attribute("user"));
            ((StorageServer) database).DAO.xPath.put(Channel);
            return "created " + name + " " + xpath;
        });

        get("/show", (request, response) -> {
            String channel = request.queryParams("channel");
            if(channel==null) {
                response.status(404);
                return "channel does not exist";
            }
            Map xPathMap = ((StorageServer) database).DAO.xPath.map();
            if (!xPathMap.containsKey(channel)) {
                response.status(404);
                return "channel does not exist";
            }
            Channel Channel = (Channel)xPathMap.get(channel);

            logger.info("Channel = " + Channel.getName() + Channel.getxPath());
            String s = "<div class=”channelheader”> <h2>"  + "Channel name: " + Channel.getName() +
                    "    Created by: " + Channel.getCreatedBy() + "</h2></div class=”channelheader”>";

            for(int Docid : Channel.getDocumentIdSet()) {
                Document doc = ((StorageServer) database).DAO.documentByCid.get(Docid);
                DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
                String crawleddate = dateFormat.format(doc.getLastAccessTime());
                String[] s_date = crawleddate.split(" ");

                s += "Crawled on: " + s_date[0] + "T" + s_date[1] + " Location: " + "<a href=" + doc.getUrl() +">"+ doc.getUrl()+"</a>";
                s += "<div class=”document”><textarea rows=\"30%\" cols=\"90%\" style=\"border:solid;\">\n" + doc.getContent() + "</textarea></div class=”document”>";
            }
            return s;
        });

        awaitInitialization();
    }
}

