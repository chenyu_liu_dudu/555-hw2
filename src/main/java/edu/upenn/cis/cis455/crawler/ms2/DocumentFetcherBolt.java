package edu.upenn.cis.cis455.crawler.ms2;

import edu.upenn.cis.cis455.crawler.utils.GLOBAL;
import edu.upenn.cis.cis455.crawler.utils.PageQueue;
import edu.upenn.cis.cis455.crawler.utils.RobotFileChecker;
import edu.upenn.cis.cis455.crawler.utils.RobotInfo;
import edu.upenn.cis.cis455.helperFunction;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageServer;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;


import java.io.IOException;
import java.util.*;

import static edu.upenn.cis.cis455.crawler.utils.RobotFileChecker.pickMyRobot;

public class DocumentFetcherBolt implements IRichBolt {
    static Logger logger = LogManager.getLogger(DocumentFetcherBolt.class);
    Fields schema = new Fields("htmlText", "url", "contentType");

    String executorId = UUID.randomUUID().toString().substring(0, 5);
    StorageServer database;

    /**
     * This is where we send our output stream
     */
    private OutputCollector collector;

    /**
     * Called when a bolt is about to be shut down
     */
    @Override
    public void cleanup() {

    }

    /**
     * performs GET robots.txt/ HEAD / GET request based on the state of the crawler
     *
     * @param input
     */
    @Override
    public void execute(Tuple input) {
        /**
         * TODO if page is in pageStateMap, then send head request to determine if we should visit the page
         * TODO else check the last modified date and send a GET request.
         */
        String url = input.getStringByField("url");
        RobotFileChecker my_robot = null;

        if(url == null) return;
        if (!RobotInfo.getRobotsMap().containsKey(helperFunction.generateRootUrl(url))) {
            //TODO send GET robot file request
            System.out.println("\n***Sending GET ROBOT request to " + url);
            try{
                my_robot = pickMyRobot(GLOBAL.USER_AGENT, url);
            } catch (NullPointerException e) {
                logger.error("no robot file exist for this url");
            }
            RobotInfo.getRobotsMap().put(helperFunction.generateRootUrl(url), my_robot);
            PageQueue.pagesToVisit().add(url);
        }
        else if(PageQueue.pageStatsHashMap().containsKey(url)) {
            //TODO send a GET request
            PageStats pageStats = PageQueue.pageStatsHashMap().get(url);
            int documentId = database.documentIsSeen(url);
            if(documentId != -1
                    && !database.documentModifiedSinceCrawled(documentId, pageStats.lastModified)
                    ) {
                System.out.println("\n***Fetch From DB for " + url);
                collector.emit(new Values<>(database.getDocument(url), url, pageStats.contentType));
            }
            else {
                System.out.println("\n***Sending GET request to " + url);
                String rawHtml = helperFunction.fetchDoc(url);
                if(rawHtml!=null) { System.out.println("**Downloading** Received web page at " + url); }
                if (database.DAO.cidByHash.contains(helperFunction.md5Hash(rawHtml))) {
                    System.out.println("!!!hash collision!!!");
                }
                database.addDocument(url, rawHtml, pageStats.contentType);
                collector.emit(new Values<>(rawHtml, url, pageStats.contentType));
                my_robot = RobotInfo.getRobotsMap().get(helperFunction.generateRootUrl(url));
                my_robot.setLastAccessTime(new Date().getTime());

            }
            PageQueue.pageVisited().add(url);

        }
        else {
            //TODO send a HEAD request
            Connection.Response response = null;
            try {
                System.out.println("\n***Sending HEAD request to " + url);
                response = Jsoup.connect(url).method(Connection.Method.HEAD).header("User-Agent", "cis455crawler").execute();
            } catch (IOException e) {
                logger.error(e.getMessage());
                return;
            }
            PageStats pageStats = new PageStats(response, url);
            System.out.println(pageStats.toString());
            if (pageStats.pageSize > GLOBAL.size ) {
                System.out.println("page size exceed limit");
                return; //skip the page if pagesize and content type does not satisfy requirement
            }
            PageQueue.pageStatsHashMap().put(url, pageStats);
            PageQueue.pagesToVisit().add(url);
            my_robot = RobotInfo.getRobotsMap().get(helperFunction.generateRootUrl(url));
            my_robot.setLastAccessTime(new Date().getTime());

        }


    }

    /**
     * Called when this task is initialized
     *
     * @param stormConf
     * @param context
     * @param collector
     */
    @Override
    public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
        this.database = StorageFactory.getDatabaseInstance();


    }

    /**
     * Called during topology creation: sets the output router
     *
     * @param router
     */
    @Override
    public void setRouter(IStreamRouter router) {
        this.collector.setRouter(router);
    }

    /**
     * Get the list of fields in the stream tuple
     *
     * @return
     */
    @Override
    public Fields getSchema() {
        return schema;
    }

    @Override
    public String getExecutorId() {
        return executorId;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(schema);
    }

    public static boolean contentTypeValid(String contentType) {
        Set<String> allowed =  new HashSet<String>(){{
            add("text/html");
            add("text/xml");
            add("application/xml");
        }};
        return allowed.contains(contentType) || contentType.endsWith("+xml");
    }

}
