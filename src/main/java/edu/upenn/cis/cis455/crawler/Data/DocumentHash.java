package edu.upenn.cis.cis455.crawler.Data;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

@Entity
public class DocumentHash {
    @PrimaryKey
    private String md5;
    private int contenId;

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public int getContenId() {
        return contenId;
    }

    public void setContenId(int contenId) {
        this.contenId = contenId;
    }
}
