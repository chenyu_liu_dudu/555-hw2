package edu.upenn.cis.cis455.crawler.ms2;

import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageServer;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import java.util.Map;
import java.util.UUID;

public class DOMParserBolt implements IRichBolt {
    static Logger logger = LogManager.getLogger(DOMParserBolt.class);
    Fields schema = new Fields("event", "documentId");
    String executorId = "DOMParserBolt:"+UUID.randomUUID().toString().substring(0, 5);
    StorageServer database;

    /**
     * This is where we send our output stream
     */
    private OutputCollector collector;

    /**
     * Called when a bolt is about to be shut down
     */
    @Override
    public void cleanup() {

    }

    /**
     * parse a document into single events that is in the form of tag1/tag2/tag3=value
     * @param input
     */
    @Override
    public void execute(Tuple input) {
        String documentText = input.getStringByField("htmlText");
        String contentType = input.getStringByField("contentType");
        String url = input.getStringByField("url");
        int documentId = this.database.getDocumentObject(url).getId();
        Document doc;
        if (contentType.contains("text/html")){
            doc = Jsoup.parse(documentText, url, Parser.htmlParser());
            System.out.println("\n***html DOM parsing");
        }  else {
            doc = Jsoup.parse(documentText, url, Parser.xmlParser());
            System.out.println("\n***xml DOM parsing");

        }
        Elements elements = doc.getAllElements();
        for (Element element : elements) {
            if (!element.ownText().isEmpty()) {
                StringBuilder path = new StringBuilder(element.nodeName());
                String value = element.ownText();
                Elements parentElems = element.parents();
                for (Element el : parentElems) {
                    path.insert(0, el.nodeName() + '/');
                }
                String event = path + "=" + value;
                collector.emit(new Values<>(event, documentId));
            }
        }


    }


    /**
     * Called when this task is initialized
     *
     * @param stormConf
     * @param context
     * @param collector
     */
    @Override
    public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
        this.database = StorageFactory.getDatabaseInstance();
    }

    /**
     * Called during topology creation: sets the output router
     *
     * @param router
     */
    @Override
    public void setRouter(IStreamRouter router) {
        this.collector.setRouter(router);
    }

    /**
     * Get the list of fields in the stream tuple
     *
     * @return
     */
    @Override
    public Fields getSchema() {
        return schema;
    }

    @Override
    public String getExecutorId() {
        return executorId;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(schema);
    }

}
