package edu.upenn.cis.cis455.crawler.utils;

import edu.upenn.cis.cis455.crawler.ms2.PageStats;

import java.util.*;

public class PageQueue {
    private static List<String> pagesToVisit = new LinkedList<String>();
    private static Set<String> pagesVisited = new HashSet<String>();
    private static Map<String, PageStats> pageStatsHashMap = new HashMap<>();

    private PageQueue()
    {
        // private constructor
    }
    public static synchronized List<String> pagesToVisit(){
        return pagesToVisit;
    }

    public static synchronized Set<String> pageVisited() {
        return pagesVisited;
    }

    public static synchronized Map<String, PageStats> pageStatsHashMap() {
        return pageStatsHashMap;
    }

}
