package edu.upenn.cis.cis455.crawler.Data;


import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.SecondaryKey;
import edu.upenn.cis.cis455.helperFunction;

import java.util.Date;

import static com.sleepycat.persist.model.Relationship.MANY_TO_ONE;

@Entity
public class Document {

    @SecondaryKey(relate=MANY_TO_ONE)
    private String url;
    @PrimaryKey
    private int id;
    private String content;
    private String contentType;



    private Date lastAccessTime;

    public Date getLastAccessTime() {
        return lastAccessTime;
    }

    public void setLastAccessTime(Date lastAccessTime) {
        this.lastAccessTime = lastAccessTime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String toString() {
        return "[id = " + id +
                "\tcontent hash= " + helperFunction.md5Hash(content)+
                "\nurl = " + url +
                "\tcontentType = " + contentType +
                "\tlastAccessTime = " + lastAccessTime+"]\n";
    }
}
