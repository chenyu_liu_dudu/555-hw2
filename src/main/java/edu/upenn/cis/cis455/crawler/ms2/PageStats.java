package edu.upenn.cis.cis455.crawler.ms2;

import edu.upenn.cis.cis455.helperFunction;
import org.jsoup.Connection;

import java.util.Date;

public class PageStats {
    Date lastModified;
    String contentType;
    int pageSize;
    String url;

    public PageStats(Connection.Response response, String url) {
        this.lastModified = helperFunction.parseDateString(response.header("Last-Modified"));
        this.contentType = response.header("Content-Type");
        this.pageSize = Integer.parseInt(response.header("Content-Length"));
        this.url = url;
    }

    public String toString() {
        return "lastmodified " + this.lastModified.toString() + " contentType " + this.contentType + " pagesize " + this.pageSize + " url " + this.url;
    }

}
