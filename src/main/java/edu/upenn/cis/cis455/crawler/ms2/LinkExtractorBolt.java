package edu.upenn.cis.cis455.crawler.ms2;

import edu.upenn.cis.cis455.crawler.utils.PageQueue;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Map;
import java.util.UUID;

public class LinkExtractorBolt implements IRichBolt {
    static Logger logger = LogManager.getLogger(LinkExtractorBolt.class);
    private OutputCollector collector;
    Fields schema = new Fields("url");
    String executorId = "LinkExtractorBolt:"+UUID.randomUUID().toString().substring(0, 5);


    /**
     * Called when a bolt is about to be shut down
     */
    @Override
    public void cleanup() {

    }

    /**
     * Processes a tuple
     * extract all links from html document
     * @param input
     */
    @Override
    public void execute(Tuple input) {
        String rawHtml = input.getStringByField("htmlText");
        String contentType = input.getStringByField("contentType");
        if (!contentType.contains("text/html")) return; //TODO noted
        if(rawHtml==null) return;
        String url = input.getStringByField("url");
        Elements linksOnPage = Jsoup.parse(rawHtml,url).select("a");
        System.out.println("found [" + linksOnPage.size() + "] links on page size = " );
        logger.info("Found (" + linksOnPage.size() + ") linksFoundOnPage");
       //TODO only html link should be crawled
        for(Element link : linksOnPage)
        {
            String absHref = link.attr("abs:href"); // "http://jsoup.org/"
            PageQueue.pagesToVisit().add(absHref);
        }
    }

    /**
     * Called when this task is initialized
     *
     * @param stormConf
     * @param context
     * @param collector
     */
    @Override
    public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
    }

    /**
     * Called during topology creation: sets the output router
     *
     * @param router
     */
    @Override
    public void setRouter(IStreamRouter router) {
        this.collector.setRouter(router);
    }

    /**
     * Get the list of fields in the stream tuple
     *
     * @return
     */
    @Override
    public Fields getSchema() {
        return schema;
    }

    @Override
    public String getExecutorId() {
        return executorId;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(schema);
    }
}
