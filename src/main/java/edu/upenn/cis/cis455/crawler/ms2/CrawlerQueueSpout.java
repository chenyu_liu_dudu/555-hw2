package edu.upenn.cis.cis455.crawler.ms2;

import edu.upenn.cis.cis455.crawler.utils.GLOBAL;
import edu.upenn.cis.cis455.crawler.utils.PageQueue;
import edu.upenn.cis.cis455.crawler.utils.RobotFileChecker;
import edu.upenn.cis.cis455.crawler.utils.RobotInfo;
import edu.upenn.cis.cis455.helperFunction;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.spout.IRichSpout;
import edu.upenn.cis.stormlite.spout.SpoutOutputCollector;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Values;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.UUID;

import static edu.upenn.cis.cis455.crawler.utils.RobotFileChecker.pickMyRobot;

public class CrawlerQueueSpout implements IRichSpout {
    static Logger log = LogManager.getLogger(CrawlerQueueSpout.class);

    String executorId = UUID.randomUUID().toString().substring(0, 5);


    /**
     * The collector is the destination for tuples; you "emit" tuples there
     */
    SpoutOutputCollector collector;

    Logger logger = LogManager.getLogger(CrawlerQueueSpout.class);

    public CrawlerQueueSpout() {
        log.debug("Starting CrawlerQueueSpout");

    }

    /**
     * Called when a task for this component is initialized within a
     * worker on the cluster. It provides the spout with the environment
     * in which the spout executes.
     *
     * @param config    The Storm configuration for this spout. This is
     *                  the configuration provided to the topology merged in
     *                  with cluster configuration on this machine.
     * @param topo
     * @param collector The collector is used to emit tuples from
     *                  this spout. Tuples can be emitted at any time, including
     *                  the open and close methods. The collector is thread-safe
     *                  and should be saved as an instance variable of this spout
     */
    @Override
    public void open(Map<String, String> config, TopologyContext topo, SpoutOutputCollector collector) {
        this.collector = collector;

    }

    /**
     * Called when an ISpout is going to be shutdown.
     * There is no guarantee that close will be called, because the
     * supervisor kill -9’s worker processes on the cluster.
     */
    @Override
    public void close() {

    }

    /**
     * When this method is called, Storm is requesting that the Spout emit
     * tuples to the output collector. This method should be non-blocking,
     * so if the Spout has no tuples to emit, this method should return.
     */
    @Override
    public void nextTuple() {
        String nextUrl = null;
        do
        {

            try{
                synchronized (PageQueue.pagesToVisit()) {
                    nextUrl = PageQueue.pagesToVisit().remove(0);
                }
            } catch (IndexOutOfBoundsException e) {
                Thread.yield();
            }

        } while(PageQueue.pageVisited().contains(nextUrl));

        if(nextUrl==null) return;
        else if(clearToCrawl(nextUrl)) {//check if url is in robot allow list
            this.collector.emit(new Values<Object>(nextUrl));
        }
    }

    @Override
    public void setRouter(IStreamRouter router) {
        this.collector.setRouter(router);
    }

    @Override
    public String getExecutorId() {
        return executorId;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("url"));
    }

    private synchronized boolean clearToCrawl(String currentUrl) {
        RobotFileChecker my_robot = null;
        String rootUrl = helperFunction.generateRootUrl(currentUrl);
        if(RobotInfo.getRobotsMap().containsKey(rootUrl)) {
            my_robot = RobotInfo.getRobotsMap().get(rootUrl);
        } else {
            try{
                my_robot = pickMyRobot(GLOBAL.USER_AGENT, currentUrl);
                RobotInfo.getRobotsMap().put(rootUrl, my_robot);
            } catch (NullPointerException e) {
                logger.info("no robot file exist for this url");
            }
        }

        if (my_robot != null) {
            if(!my_robot.isDelayPass()) {
                PageQueue.pagesToVisit().add(currentUrl);
                return false;
            }
            if(my_robot.urlIsDisallowed(currentUrl)) {
                return false;
            }
        }
        return true;
    }
}
