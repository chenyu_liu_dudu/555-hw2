package edu.upenn.cis.cis455.crawler.Data;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

@Entity

public class User {
    @PrimaryKey
    private String userName;
    private int Id;
    private String password;

    public String getUserName() {
        return userName;
    }

    public int getId() {
        return Id;
    }

    public String getHashedPassword() {
        return password;
    }

    public void setId(int id) {
        Id = id;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setHashedPassword(String hashedPassword) {
        this.password = hashedPassword;
    }

}
