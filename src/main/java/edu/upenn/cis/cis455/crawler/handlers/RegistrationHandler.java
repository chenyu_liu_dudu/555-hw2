package edu.upenn.cis.cis455.crawler.handlers;

import edu.upenn.cis.cis455.crawler.utils.GLOBAL;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.StorageServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import spark.*;

public class RegistrationHandler implements Route {
    StorageServer db;
    Logger logger = LogManager.getLogger(RegistrationHandler.class);

    public RegistrationHandler(StorageInterface db) {
        this.db = (StorageServer) db;
    }


    @Override
    public String handle(Request req, Response resp) throws HaltException {
        String user = req.queryParams("username");
        String pass = req.queryParams("password");

        logger.info("Register request for user " + user + " and password " + pass);
        if (db.getUserByUsername(user)!=null) {
            logger.info("username exist in database");
//            resp.redirect("/register.html");
            resp.status(409);
            return "Please use a different user name";
        }else {
            db.addUser(user, pass);
            Session session = req.session();
            session.maxInactiveInterval(GLOBAL.EXPIRE);
            session.attribute("user", user);
            session.attribute("password", pass);
            resp.status(200);
            return "<form method=\"GET\" action=\"/\">\n" +
                    "<input type=\"submit\" value=\"home\"/>\n" +
                    "</form>";
        }

    }


}
