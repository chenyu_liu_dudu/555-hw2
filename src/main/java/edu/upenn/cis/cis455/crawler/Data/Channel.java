package edu.upenn.cis.cis455.crawler.Data;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


@Entity
public class Channel {
    @PrimaryKey
    private String name;
    private String xPath;
    public Set<Integer> documentIdSet = new HashSet<>();
    public Date createTime;

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String createdBy;

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setxPath(String xPath) {
        this.xPath = xPath;
    }

    public void setDocumentIdSet(Set<Integer> documentIdSet) {
        this.documentIdSet = documentIdSet;
    }


    public String getxPath() {
        return xPath;
    }

    public Set<Integer> getDocumentIdSet() {
        return documentIdSet;
    }
}
