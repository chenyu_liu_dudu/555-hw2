package edu.upenn.cis.cis455.crawler.handlers;

import spark.*;

public class LogoutHandler implements Route {


    @Override
    public String handle(Request req, Response resp) throws HaltException {
        System.err.println("handling logout");
        for(String s : req.session().attributes()) {
            System.err.println("s = " + s);
        }

        System.err.println("Logged out!");
        Session session = req.session();
        session.invalidate();
        resp.redirect("/login-form.html");
        return "";
    }
}
