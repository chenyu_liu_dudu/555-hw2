package edu.upenn.cis.cis455.crawler.utils;

import edu.upenn.cis.cis455.helperFunction;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;

public class CrawlerLeg
{
    Logger logger = LogManager.getLogger(CrawlerLeg.class);
    private List<String> linksFoundOnPage = new LinkedList<String>();
    private static int maxPageSize;
    StorageServer database = StorageFactory.getDatabaseInstance();
    public CrawlerLeg(int maxPageSize) {
        this.maxPageSize = 1000000 * maxPageSize;
    }


    /**
     * makes HTTP Request, checks the response and collect all linksFoundOnPage
     * @param url
     * @throws Exception
     */
    public void crawl(String url) throws Exception
    {
        int documentId = database.documentIsSeen(url);
        Connection.Response response = null;
        try {
            response = Jsoup.connect(url).method(Connection.Method.HEAD).header("User-Agent", "cis455crawler").execute();
            logger.info("***Sending HEAD request to " + url);
        } catch (IOException e) {
            logger.error(e.getMessage());
            throw e;
        }

        if (Integer.parseInt(response.header("Content-Length")) > maxPageSize
                || !response.header("Content-Type").equals("text/html")) {
            return;
        }

        if (documentId != -1 // if document is seen
            && !database.documentModifiedSinceCrawled(documentId, response.header("Last-Modified"))) // if document not modified
        {
            if (database.DAO.cidByHash.contains(database.getDocument(url))) { throw new Exception(); }

            logger.info("document " + url + "is  unmodified");
            Document htmlDocument = Jsoup.parse(database.getDocument(url));
            Elements linksOnPage = htmlDocument.select("a[href]");
            for(Element link : linksOnPage)
            {
                System.out.println("adding " + url + link.attr("href"));
                this.linksFoundOnPage.add(url + link.attr("href"));
            }
        }
        else {
            String rawHtml = helperFunction.fetchDoc(url);
            if(rawHtml!=null) { System.out.println("\n**Downloading** Received web page at " + url); }
            else if (database.DAO.cidByHash.contains(rawHtml)) { throw new Exception(); }
            else { throw new Exception(); }

            Elements linksOnPage = Jsoup.parse(rawHtml).select("a[href]");
            logger.info("Found (" + linksOnPage.size() + ") linksFoundOnPage");
            database.addDocument(url, rawHtml, response.contentType());
            for(Element link : linksOnPage)
            {
                this.linksFoundOnPage.add(url + link.attr("href")); //add new linksFoundOnPage to be crawled
            }
        }
    }

    public List<String> getLinksFoundOnPage()
    {
        return this.linksFoundOnPage;
    }

}