package edu.upenn.cis.cis455.crawler.ms2;

import edu.upenn.cis.cis455.crawler.Data.Channel;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageServer;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PathMatcherBolt implements IRichBolt {

    static Logger logger = LogManager.getLogger(PathMatcherBolt.class);
    Fields schema = new Fields("matchVec", "documentId");

    String executorId = UUID.randomUUID().toString().substring(0, 5);
    StorageServer database;
    /**
     * This is where we send our output stream
     */
    private OutputCollector collector;


    /**
     * Called when a bolt is about to be shut down
     */
    @Override
    public void cleanup() {

    }

    /**
     * Processes a tuple (event, documentId)
     * if event matches xPath, then add documentId to xPath matching set
     * @param input
     */
    @Override
    public void execute(Tuple input) {
        String event = input.getStringByField("event");
        int docId = input.getIntegerByField("documentId");
        for (Channel p : this.database.DAO.xPath.map().values()) {
             boolean match = isMatch(p.getxPath(), event);
             if (match) {
                 logger.info("found a matching channel on docId" + docId);
                 logger.info("event{"+ event+"}");
                 logger.info("channel{"+ p.getxPath()+"}");
                 Channel x = this.database.DAO.xPath.get(p.getName());
                 x.getDocumentIdSet().add(docId);
                 this.database.DAO.xPath.put(x);
             }
        }
//        collector.emit(new Values<>(Arrays.toString(matchBits), docId));
    }

    /**
     * Called when this task is initialized
     *
     * @param stormConf
     * @param context
     * @param collector
     */
    @Override
    public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
        this.database = StorageFactory.getDatabaseInstance();
    }

    /**
     * Called during topology creation: sets the output router
     *
     * @param router
     */
    @Override
    public void setRouter(IStreamRouter router) {
        this.collector.setRouter(router);
    }

    /**
     * Get the list of fields in the stream tuple
     *
     * @return
     */
    @Override
    public Fields getSchema() {
        return schema;
    }

    @Override
    public String getExecutorId() {
        return executorId;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(schema);

    }

    /**
     * check if event patches xpath
     * @param xpath
     * @param event
     * @return
     */
    public static boolean isMatch(String xpath, String event) {
        String[] path = xpath.substring(1).split("/");
        String[] ev = event.split("/");
        if(ev.length < path.length) return false;
        for(int i=0;i<path.length;i++) {
            if(path[i].contains("contains")) {
                String eventTag = eventTag(ev[i]);
                String pathTag = pathTag(path[i]);
                if (!eventTag.toLowerCase().equals(pathTag.toLowerCase())) return false;
                String eventText = eventText(ev[i]);
                String pathText = pathText(path[i]);
                System.out.println("eventText =" + eventText + "\npathText ="+pathText + "\npath[i] =" + path[i] );
                if(eventText.isEmpty() && !pathText.isEmpty()) {
                    return false;
                }
                else if(eventText.isEmpty() && pathText.isEmpty()){
                    continue;
                }
                if (!eventText.toLowerCase().contains(pathText.toLowerCase())) return false;

            }
            else if(path[i].contains("text()")) {
                String eventTag = eventTag(ev[i]);
                String pathTag = pathTag(path[i]);
                if (!eventTag.toLowerCase().equals(pathTag.toLowerCase())) return false;

                String eventText = eventText(ev[i]);
                String pathText = pathText(path[i]);
                if (eventText != null && pathText == null) { //
                    return false;
                }
                else if(eventText == null && pathText != null) {
                    return false;
                }
                else if(eventText == null && pathText == null){
                    continue;
                }
                if (!pathText.toLowerCase().equals(eventText.toLowerCase())) return false;
            }
            else if(!path[i].equals(ev[i])) {
                String eventTag = eventTag(ev[i]);
                String pathTag = pathTag(path[i]);
                if (!eventTag.toLowerCase().equals(pathTag.toLowerCase())) {
                    return false;
                }
            }
        }
        return true;
    }

    public static String eventTag(String event) {
        String eventPattern = "(.*)=";
        Pattern r = Pattern.compile(eventPattern);
        Matcher m = r.matcher(event);
        String value = event;
        if(m.find()) {
            value = m.group(1);
        }
        return value;
    }
    public static String pathTag(String path) {
        String eventPattern = "(.*)\\[";
        Pattern r = Pattern.compile(eventPattern);
        Matcher m = r.matcher(path);
        String value = path;
        if(m.find()) {
            value = m.group(1);
        }
        return value;
    }

    public static String pathText(String event) {
        String eventPattern = "\"(.*)\"";
        Pattern r = Pattern.compile(eventPattern);
        Matcher m = r.matcher(event);
        String value = "";
        if(m.find()) {
            value = m.group(1);
        }
        return value;
    }

    public static String eventText(String event) {
        String eventPattern = "=(.*)";
        Pattern r = Pattern.compile(eventPattern);
        Matcher m = r.matcher(event);
        String value = "";
        if(m.find()) {
            value = m.group(1);
        }
        return value;
    }
}
