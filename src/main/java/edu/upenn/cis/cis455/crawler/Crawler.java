package edu.upenn.cis.cis455.crawler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.upenn.cis.cis455.crawler.Data.Channel;
import edu.upenn.cis.cis455.crawler.ms2.*;
import edu.upenn.cis.cis455.crawler.utils.*;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageServer;
import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.LocalCluster;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;


public class Crawler implements CrawlMaster {
    Logger logger = LogManager.getLogger(Crawler.class);

    ///// TODO: you'll need to flesh all of this out. You'll need to build a thread
    // pool of CrawlerWorkers etc.

    public static final int NUM_WORKERS = 10;
    public boolean isDone = false;
    public StorageServer database;

    private static final String CrawlerQueue_SPOUT = "CrawlerQueue_SPOUT";
    private static final String DOC_BOLT = "DOC_BOLT";
    private static final String LinkExtractor_BOLT = "LinkExtractor_BOLT";
    public Crawler(StorageServer database) {
        this.database = database;
    }

    /**
     * Main thread
     */
    public void start() {
        PageQueue.pagesToVisit().add(GLOBAL.startUrl);
        Config config = new Config();

        CrawlerQueueSpout spout = new CrawlerQueueSpout();
        DocumentFetcherBolt docbolt = new DocumentFetcherBolt();
        LinkExtractorBolt linkbolt = new LinkExtractorBolt();
        DOMParserBolt domParserBolt = new DOMParserBolt();
        PathMatcherBolt pathMatcherBolt = new PathMatcherBolt();

        // wordSpout ==> countBolt ==> MongoInsertBolt
        TopologyBuilder builder = new TopologyBuilder();

        // Only one source ("spout") for the words
        builder.setSpout(CrawlerQueue_SPOUT, spout, 3);

        builder.setBolt(DOC_BOLT, docbolt, 3).shuffleGrouping(CrawlerQueue_SPOUT);

        builder.setBolt(LinkExtractor_BOLT, linkbolt, 2).shuffleGrouping(DOC_BOLT);

        builder.setBolt("DOM_BOLT", domParserBolt, 2).shuffleGrouping(DOC_BOLT);

        builder.setBolt("PATH_MATCH_BOLT", pathMatcherBolt, 5).shuffleGrouping("DOM_BOLT");

        LocalCluster cluster = new LocalCluster();
        Topology topo = builder.createTopology();

        ObjectMapper mapper = new ObjectMapper();
        try {
            String str = mapper.writeValueAsString(topo);

            System.out.println("The StormLite topology is:\n" + str);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        cluster.submitTopology("test", config,
                builder.createTopology());

        while (!this.isDone())
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        System.out.println(" size of queue = " +  PageQueue.pageVisited().size() + "\nPageQueue.pagesToVisit()" +
                PageQueue.pagesToVisit().toString());

        database.printDocumentInDb();

        for(Channel p : database.DAO.xPath.map().values()) {
            System.out.println(p.getName() + ":" + p.getxPath());
            System.out.println(p.getDocumentIdSet().toString());

        }
        cluster.killTopology("test");
        cluster.shutdown();
//        database.close();

    }


    /**
     * We've indexed another document
     */
    @Override
    public void incCount() {
    }

    /**
     * Workers can poll this to see if they should exit, ie the crawl is done
     */
    @Override
    public boolean isDone() {
        return GLOBAL.count <= PageQueue.pageVisited().size() ;
    }

    /**
     * Workers should notify when they are processing an URL
     */
    @Override
    public void setWorking(boolean working) {
    }

    /**
     * Workers should call this when they exit, so the master knows when it can shut
     * down
     */
    @Override
    public void notifyThreadExited() {
    }


    /**
     * Main program: init database, start crawler, wait for it to notify that it is
     * done, then close.
     */
    public static void main(String args[]) {
        if (args.length < 3 || args.length > 5) {
            System.out.println("Usage: Crawler {start URL} {database environment path} {max doc size in MB} {number of files to index}");
            System.exit(1);
        }
        System.out.println("Crawler starting");

        GLOBAL.startUrl = args[0];
        GLOBAL.envPath = args[1];
        GLOBAL.size = Integer.valueOf(args[2]) * 1000000;
        GLOBAL.count = args.length == 4 ? Integer.valueOf(args[3]) : 100;

        System.out.println("Starting crawl of " + GLOBAL.count + " documents, starting at " + GLOBAL.startUrl + "envPath = " + GLOBAL.envPath);

        StorageServer database = (StorageServer) StorageFactory.getDatabaseInstance(GLOBAL.envPath);
        database.truncateMd5CidDatabase();
        System.out.println("\tcid map size = " + database.DAO.cidByHash.map().size());
        Crawler crawler = new Crawler(database);
        crawler.start();

        System.out.println("Done crawling!");
        System.exit(0);

    }

}
