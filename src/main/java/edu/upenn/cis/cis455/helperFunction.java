package edu.upenn.cis.cis455;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.XmlDeclaration;
import org.jsoup.parser.Parser;
import spark.utils.IOUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.TimeZone;

public class helperFunction {
    static Logger logger = LogManager.getLogger(helperFunction.class);

    /**
     *
     * @param password
     * @return sha256hash of password
     */
    public static String sha256hash(String password) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return Base64.getEncoder().encodeToString(digest.digest(password.getBytes(StandardCharsets.UTF_8)));
    }

    /**
     *
     * @param input
     * @return the MD5hash of input String
     */
    public static String md5Hash(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger no = new BigInteger(1, messageDigest);
            // Convert message digest into hex value
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

    }

    /**
     * parse a date string
     * @param t
     * @return Date object parsed from string t
     */
    public static Date parseDateString(String t) {
        if (t == null) return null;
        SimpleDateFormat sf1 = new SimpleDateFormat("EE, dd MMM yyyy HH:mm:ss zzz");
        SimpleDateFormat sf2 = new SimpleDateFormat("EEEEEEEEE, dd-MMM-yy HH:mm:ss zzz");
        SimpleDateFormat sf3 = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy");
        sf1.setTimeZone(TimeZone.getTimeZone("GMT"));
        sf2.setTimeZone(TimeZone.getTimeZone("GMT"));
        sf3.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date = null;
        try {
            date = sf1.parse(t);
        } catch (ParseException e) {

        }
        try {
            date = sf2.parse(t);
        } catch (ParseException e) {

        }
        try {
            date = sf3.parse(t);
        } catch (ParseException e) {

        }
        return date;
    }


    /**
     * fetch document from the provided url
     * @param url
     * @return document content String
     */
    public static String fetchDoc(String url) {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url)
                    .openConnection();
            System.out.println("connection is made");
            connection.setRequestProperty("User-Agent", "cis455crawler");
            int responseCode = connection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStr = connection.getInputStream();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                IOUtils.copy(inputStr, baos);
                byte[] byteArray = (baos.toByteArray());
                if (connection.getContentType().contains("xml")) {
                    Connection.Response response  = Jsoup
                            .connect(url)
                            .method(Connection.Method.GET)
                            .data("User-Agent", "cis455crawler")
                            .execute();
                    Document doc = Jsoup.parse(response.body(), "", Parser.xmlParser());
                    if (doc.childNode(0).hasAttr("encoding")) {
                        XmlDeclaration decl = (XmlDeclaration) doc.childNode(0);
                        response.charset(decl.attr("encoding"));
                        return response.body();
                    }
                    else {
                        return new String(byteArray);
                    }
                }
                else {
                    return new String(byteArray);
                }
            }
        } catch (Exception e) {
            logger.error("fetchDoc:" +e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param u
     * @return robot url of url u
     */
    public static String generateRootUrl(String u) {
        URL url;
        try{
            url = new URL(u);
            String protocol = url.getProtocol();
            String authority = url.getAuthority();
            return String.format("%s://%s", protocol, authority);
        } catch (MalformedURLException e) {
            logger.error("generateRootUrl: "+e.getMessage());
        }
        return null;
    }

    /**
     * get robots.txt file url
     * @param u
     * @return robots.txt file url
     */
    public static String generateRobotUrl(String u) {
        String roboUrl = null;
        try {
            URL url = new URL(u);
            String protocol = url.getProtocol();
            String authority = url.getAuthority();
            roboUrl = String.format("%s://%s", protocol, authority) + "/robots.txt";
        } catch (MalformedURLException e) {
            logger.error("generateRobotUrl:" +e.getMessage());
        }
        return roboUrl;
    }



    /**
     * fetch robots.txt file
     * @param url
     * @return robot.txt file
     */
    public static String fetchRobotsFile(String url) {
        String robotUrl = generateRobotUrl(url);
        String res = null;
        try {
            Connection.Response resp = Jsoup.connect(robotUrl).method(Connection.Method.GET)
                    .header("User-Agent", "cis455crawler")
                    .execute();
            res = resp.body();
        } catch (IOException e) {
            logger.error("fetchRobotsFile: "+e.getMessage());
        }
        return res;
    }


}
