package edu.upenn.cis.cis455.storage;


import edu.upenn.cis.cis455.crawler.utils.GLOBAL;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StorageFactory {
    static Logger logger  = LogManager.getLogger(StorageServer.class);
    private static StorageServer storageServer;

    public static StorageServer getDatabaseInstance(String directory) {
        logger.info("getDatabaseInstance");
        // TODO: factory object, instantiate your storage server
        if (storageServer == null) {
            storageServer = new StorageServer(directory);
        }
        return storageServer;
    }

    public static StorageServer getDatabaseInstance() {
        return storageServer;
    }
}
