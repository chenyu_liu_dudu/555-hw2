package edu.upenn.cis.cis455.storage;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.SecondaryIndex;
import edu.upenn.cis.cis455.crawler.Data.Channel;
import edu.upenn.cis.cis455.crawler.Data.Document;
import edu.upenn.cis.cis455.crawler.Data.DocumentHash;
import edu.upenn.cis.cis455.crawler.Data.User;

public class DAO {
    //User Accessors
    public PrimaryIndex<String, User> userByName;
    public PrimaryIndex<String, DocumentHash> cidByHash;
    public PrimaryIndex<Integer, Document> documentByCid;
    public SecondaryIndex<String, Integer, Document> documentByUrl;
    public PrimaryIndex<String, Channel> xPath;

    // Open the indices
    public DAO(EntityStore store) throws DatabaseException {

        userByName = store.getPrimaryIndex(String.class, User.class);
        cidByHash = store.getPrimaryIndex(String.class, DocumentHash.class); //seenhash to contentid
        documentByCid = store.getPrimaryIndex(Integer.class, Document.class);//contentid to document
        documentByUrl = store.getSecondaryIndex(documentByCid, String.class, "url");
        xPath = store.getPrimaryIndex(String.class, Channel.class);
    }
}

