package edu.upenn.cis.cis455.storage;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.persist.EntityCursor;
import edu.upenn.cis.cis455.crawler.Data.*;
import edu.upenn.cis.cis455.helperFunction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.Date;

import static edu.upenn.cis.cis455.helperFunction.md5Hash;
import static edu.upenn.cis.cis455.helperFunction.sha256hash;

public class StorageServer implements StorageInterface {
    static Logger logger = LogManager.getLogger(StorageFactory.class);

    public DAO DAO;

    private static MyDbEnv myDbEnv = new MyDbEnv(); // Encapsulates the environment and data store.

    public StorageServer(String directory) {
        File myDbEnvPath = new File(directory);
        if (myDbEnvPath.exists() || myDbEnvPath.mkdirs()) {
            myDbEnv.setup(myDbEnvPath, false);
            DAO = new DAO(myDbEnv.getEntityStore());
        }

    }

    public void truncateDocumentDatabase() {
        DAO.documentByCid.map().clear();
        DAO.documentByUrl.map().clear();
    }

    public void truncateMd5CidDatabase() {
        DAO.cidByHash.map().clear();
    }

    public void printUserInDb() throws DatabaseException {
        EntityCursor<User> items = DAO.userByName.entities();
        try {
            for (User item : items) {
                System.out.println(item.getUserName() + " = " + item.getId());
            }
        } finally {
            items.close();
        }
    }

    public void printDocumentInDb() throws DatabaseException {
        EntityCursor<Document> items = DAO.documentByCid.entities();
        for (Document item : items) {
            System.out.println(item.toString());
        }
        items.close();
    }

    public void addIdToChannel(int id, String channelName) {
        DAO.xPath.get(channelName).getDocumentIdSet().add(id);
    }

    /**
     * How many documents so far?
     */
    @Override
    public int getCorpusSize() {
        return 0;
    }

    /**
     * Add a new document, getting its ID
     *
     * @param url
     * @param documentContents
     */
    @Override
    public int addDocument(String url, String documentContents) {
        System.out.println("adding " + url + " to database");
        int id = DAO.documentByCid.map().size() +1;

        Document d = new Document();
        d.setUrl(url);
        d.setId(id);
        d.setContent(documentContents);

        DocumentHash documentHash = new DocumentHash();
        documentHash.setMd5(md5Hash(documentContents));
        documentHash.setContenId(id);

        DAO.documentByCid.put(d);
        DAO.cidByHash.put(documentHash);
        return id;
    }

    public int addDocument(String url, String documentContents, String contentType) {
        System.out.println("adding " + url + " to database");
        int id = DAO.documentByCid.map().size() +1;

        Document d = new Document();
        d.setUrl(url);
        d.setId(id);
        d.setContentType(contentType);
        d.setContent(documentContents);
        d.setLastAccessTime(new Date());

        DocumentHash documentHash = new DocumentHash();
        documentHash.setMd5(md5Hash(documentContents));
        documentHash.setContenId(id);
        DAO.documentByCid.put(d);
        DAO.cidByHash.put(documentHash);
        //TODO update old document if it exist
        return id;
    }

    public int documentIsSeen(String url) {
        Document d = DAO.documentByUrl.get(url);
        if( d != null) return d.getId();
        else return -1;
    }
    public Document getDocumentObject(String url){
        return DAO.documentByUrl.get(url);
    }

    public boolean documentModifiedSinceCrawled(int contentId, String lastModifiedDate) {
        Document document = DAO.documentByCid.get(contentId);
        return document.getLastAccessTime().before(helperFunction.parseDateString(lastModifiedDate));
    }


    public boolean documentModifiedSinceCrawled(int contentId, Date lastModifiedDate) {
        Document document = DAO.documentByCid.get(contentId);
        return document.getLastAccessTime().before(lastModifiedDate);
    }




    public User getUserByUsername(String userName) {
        User u = DAO.userByName.get(userName);
        return u;
    }

    /**
     * Retrieves a document's contents by URL
     *
     * @param url
     */
    @Override
    public String getDocument(String url) {
        Document d = DAO.documentByUrl.get(url);
        if(d==null) return null;
        return d.getContent();
    }

    /**
     * Adds a user and returns an ID
     * UserId should be Unique
     * @param username
     * @param password
     */
    @Override
    public synchronized int addUser(String username, String password) {
        int id = DAO.userByName.map().size() + 1;

        User newUser = new User();
        newUser.setId(id);
        newUser.setUserName(username);
        newUser.setHashedPassword(sha256hash(password));
        DAO.userByName.put(newUser);
        return id;
    }

    /**
     * Tries to log in the user, or else throws a HaltException
     *
     * @param username
     * @param password
     */
    @Override
    public boolean getSessionForUser(String username, String password) {
        User s = DAO.userByName.get(username);
        if (s!=null && s.getHashedPassword().equals(sha256hash(password))) {
            return true;
        }
        return false;
    }

    /**
     * Shuts down / flushes / closes the storage system
     */
    @Override
    public void close() {
        myDbEnv.close();
    }



}
