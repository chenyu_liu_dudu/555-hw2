package edu.upenn.cis.cis455.xpathengine;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XPathEngineImpl implements XPathEngine{
    public String[] xPathsList;

    /**
     * Sets the XPath expression(s) that are to be evaluated.
     *
     * @param expressions
     */
    @Override
    public void setXPaths(String[] expressions) {
        this.xPathsList = expressions;
    }

    /**
     * Event driven pattern match.
     * <p>
     * Takes an event at a time as input
     *
     * @param event notification about something parsed, from a given document
     * @return bit vector of matches to XPaths
     */
    @Override
    public boolean[] evaluateEvent(OccurrenceEvent event) {
        return new boolean[0];
    }



}
